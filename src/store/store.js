import createPersistedState from 'vuex-persistedstate';

import Vue from 'vue';
import Vuex from 'vuex';
import alumnos from './alumnos';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    baseURL: '/api',
  },
  modules: {
    alumnos,
    // almuerzos,
    // consulta,
  },
  mutations: {

  },
  actions: {

  },
  plugins: [
    createPersistedState(),
  ],
});
