export default {
    namespaced: true,
    state: {
      rut: null,
      cantidad: null,
    },
    mutations: {
      setRut(state, rut) {
        state.rut = rut;
      },
      setCantidad(state, cantidad) {
        state.cantidad = cantidad;
      },
    },
  };
  