export default {
    namespaced: true,
    state: {
      rut: null,
    },
    mutations: {
      setRut(state, rut) {
        state.rut = rut;
      },
    },
  };
  