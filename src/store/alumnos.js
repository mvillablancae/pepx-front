import HTTP from '../http';

export default {
  namespaced: true,
  state: {
    rut: null,
    email: null,
    estado: null,
  },
  actions: {
    registrar({ commit, state }) {
      return HTTP().post('/register', {
        rut: state.rut,
        email: state.email,
      }).then(() => {
        commit('setResponse', 'Usuario registrado satisfactoriamente.');
      }).catch(() => {
        commit('setResponse', 'Usuario registrado satisfactoriamente.');
      });
    },
  },
  mutations: {
    setResponse(state, estadoResponse) {
      state.estado = estadoResponse;
    },
    setRut(state, rut) {
      state.rut = rut;
    },
    setEmail(state, email) {
      state.email = email;
    },
  },
};
