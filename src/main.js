import 'vuetify/dist/vuetify.min.css';

import { sync } from 'vuex-router-sync';
import Vuetify from 'vuetify';
import '@mdi/font/css/materialdesignicons.css';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';

Vue.config.productionTip = false;

const vuetifyOptions = {};
Vue.use(Vuetify);
sync(store, router);

new Vue({
  router,
  store,
  vuetify: new Vuetify(vuetifyOptions),
  render(h) { return h(App); },
}).$mount('#app');
